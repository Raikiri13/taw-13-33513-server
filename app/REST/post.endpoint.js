import business from '../business/business.container';
import postDAO from "../DAO/postDAO";
const postEndpoint = (router) => {
    router.get('/api/posts', async (request, response, next) => {
        try {
            let result = await business.getPostManager().query();
            response.status(200).send(result);
        } catch (error) {
            console.log(error);
        }
    });

    router.post("/api/create-post",async (req, res) => {
        let post_title = String(req.body.post_title);
        let post_image = String(req.body.post_image);
        let post_contents = String(req.body.post_image);
        await postDAO.createNewOrUpdate({title: post_title, image: post_image, text: post_contents});

        res.send("Addition - " + result);
    });

};
export default postEndpoint;
